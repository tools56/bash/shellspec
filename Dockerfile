FROM ubuntu:18.04
ENV TZ Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

LABEL version="1.0"
LABEL description="Environnement de base pour execution des tests bash"

RUN apt-get update

# Composant pour docker
RUN apt-get install -y apt-transport-https
RUN apt-get install -y ca-certificates 
RUN apt-get install -y software-properties-common
RUN apt-get install -y curl

# Composant pour git
RUN apt-get install -y git

#retrieves files from the web
RUN apt-get install -y wget

RUN curl -fsSL https://git.io/shellspec | sh -s -- --yes --prefix /usr
